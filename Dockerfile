FROM ubuntu:latest

RUN mkdir /app
WORKDIR /app
COPY . /app
RUN apt-get -y update
RUN apt-get install -y python3
CMD python3 app.py